Machine learning with scikit
============================

In this tutorial, we will use Scikit library for machine learning along with Pandas, Numpy and Matplotlib. 

What is machine learning
------------------------

Machine learning is a type of artificial intelligence (AI) that provides computers with the ability to learn without being explicitly programmed. Machine learning focuses on the development of computer programs that can teach themselves to grow and change when exposed to new data.

In this tutorial, two types of machine learnings are discussed i.e. Supervised and unsupervised learning. The learnings can be further divided into two groups i.e. classification and regression. In this section, these four terms are discussed. Then, next sections shows some examples of these terms using 'scikit' library.

Supervised vs Unsupervised learning
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In supervised learning we have two types of objects i.e. data and  outcomes (labeled data). In the other words, we know the possible values of outputs e.g. tossing a coin has only two possible outcomes i.e. Head and tail.

Whereas in unsupervised learning we do not have labeled data e.g. few persons talking on phones along with some music; and we want to find the number of persons in conversation. In this case, we have no idea about number of persons talking on the phone and the type of music. So, we need to create the labels based on some speech data obtained during conversation.  


Classification vs regression
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In classification, the output has fixed number of outcomes e.g. tossing a coin has two outcomes i.e. head and tail. 

Whereas in regression, the output is the continuous variable i.e. output can have any number of values e.g. height of the people. 


Scikit Example
--------------

This section shows, some examples of various types of learning using 'scikit' library. Then in next section, Scikit, Pandas, Numpy and Matplotlib library will be used to apply machine learning algorithm on some real world data.


Supervised learning
^^^^^^^^^^^^^^^^^^^




Predictive modeling
-------------------

In this section, we will use machine learning algorithm with some real world data.

Reading data
^^^^^^^^^^^^

We will use 'titanic_train.csv' file, which has first 5 lines as follows, 

.. code-block:: python

    
    >>> with open('titanic_train.csv', 'r') as f:
    ...     for i, line in zip(range(5), f):
    ...             print(line.strip())
    ... 
    PassengerId,Survived,Pclass,Name,Sex,Age,SibSp,Parch,Ticket,Fare,Cabin,Embarked
    1,0,3,"Braund, Mr. Owen Harris",male,22,1,0,A/5 21171,7.25,,S
    2,1,1,"Cumings, Mrs. John Bradley (Florence Briggs Thayer)",
            female,38,1,0,PC 17599,71.2833,C85,C
    3,1,3,"Heikkinen, Miss. Laina",female,26,0,0,STON/O2. 3101282,7.925,,S
    4,1,1,"Futrelle, Mrs. Jacques Heath (Lily May Peel)",
            female,35,1,0,113803,53.1,C123,S



* We can read the file using pandas as follows, 

.. code-block:: python

    >>> import pandas as pd
    >>> data = pd.read_csv('titanic_train.csv')
    >>> data.tail()
         PassengerId  Survived  Pclass                                      Name  \
    886          887         0       2                     Montvila, Rev. Juozas   
    887          888         1       1              Graham, Miss. Margaret Edith   
    888          889         0       3  Johnston, Miss. Catherine Helen "Carrie"   
    889          890         1       1                     Behr, Mr. Karl Howell   
    890          891         0       3                       Dooley, Mr. Patrick   

            Sex   Age  SibSp  Parch      Ticket   Fare Cabin Embarked  
    886    male  27.0      0      0      211536  13.00   NaN        S  
    887  female  19.0      0      0      112053  30.00   B42        S  
    888  female   NaN      1      2  W./C. 6607  23.45   NaN        S  
    889    male  26.0      0      0      111369  30.00  C148        C  
    890    male  32.0      0      0      370376   7.75   NaN        Q 

* We can find the total number of not-null entries in different columns as follows. Below results shows that there are 891 passenger whereas only 714 provided their age. 

.. code-block:: python
    
    >>> data.count()
    PassengerId    891
    Survived       891
    Pclass         891
    Name           891
    Sex            891
    Age            714
    SibSp          891
    Parch          891
    Ticket         891
    Fare           891
    Cabin          204
    Embarked       889
    dtype: int64

* Below is the detail of each column, 

.. table:: Column details

    +-------------+------------------------------------------------+
    | Column      | Detail                                         |
    +=============+================================================+
    | PassengerId | id for passenger                               |
    +-------------+------------------------------------------------+
    | Survived    |  passenger survived after crash (1 or 0)       |
    +-------------+------------------------------------------------+
    | Pclass      | passenger traveling class i.e. 1st, 2nd or 3rd |
    +-------------+------------------------------------------------+
    | Name        | name of passenger                              |
    +-------------+------------------------------------------------+
    | Sex         | gender of passenger                            |
    +-------------+------------------------------------------------+
    | Age         | age of passenger                               |
    +-------------+------------------------------------------------+
    | SibSp       | Sibling infomrmation                           |
    +-------------+------------------------------------------------+
    | Parch       |                                                |
    +-------------+------------------------------------------------+
    | Ticket      | ticket number                                  |
    +-------------+------------------------------------------------+
    | Fare        | price for ticket                               |
    +-------------+------------------------------------------------+
    | Cabin       | cabin number                                   |
    +-------------+------------------------------------------------+
    | Embarked    |                                                |
    +-------------+------------------------------------------------+


.. note:: 

    Currently type of data is 'pandas dataframe', which can be converted into numpy array using 'value' command as shown below, 

    .. code-block:: python

        >>> type(data)
        <class 'pandas.core.frame.DataFrame'>
        >>> d = data.values
        >>> d
        array([[1, 0, 3, ..., 7.25, nan, 'S'],
               [2, 1, 1, ..., 71.2833, 'C85', 'C'],
               [3, 1, 3, ..., 7.925, nan, 'S'],
               ..., 
               [889, 0, 3, ..., 23.45, nan, 'S'],
               [890, 1, 1, ..., 30.0, 'C148', 'C'],
               [891, 0, 3, ..., 7.75, nan, 'Q']], dtype=object)
        >>> type(d)
        <class 'numpy.ndarray'>


.. important:: 

    Above numpy arrays values can not be fed directly to 'scikit-learn model' because of following reasons
   
    * the target variable (survival) is mixed with the input data

    * some attribute such as unique ids have no predictive values for the task

    * the values are heterogeneous (string labels for categories, integers and floating point numbers)

    * some attribute values are missing (nan: "not a number") 

Converting useful data to scikit format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. note

    We want to perform the survival-prediction after the crash. For this prediction all the information in the csv file is not useful for predictive modeling e.g. PassengerId is unique for all the person therefore it will not give any useful information for prediction.  Similarly, non-numeric values such as passenger name is not useful for survival prediction. Therefore we will extract only useful information from the data and convert it into scikit-readable format. 


.. code-block:: python
    
    >>> survived_column = data['Survived']
    >>> survived_column.dtype
    dtype('int64')
    >>> survived_column.head()
    0    0
    1    1
    2    1
    3    1
    4    0
    Name: Survived, dtype: int64



* types is used to convert the pandas dataframe to numpy array using **values** attribute. 

.. code-block:: python

    >>> type(data)
    <class 'pandas.core.frame.DataFrame'>

    >>> target = survived_column.values
    
    >>> type(target)
    <class 'numpy.ndarray'>
    
    >>> target[:5]
    array([0, 1, 1, 1, 0], dtype=int64)


* Above result will used as labels (i.e. outputs). Next, we need to create a dataset. Let us start simple and build a first model that only uses readily available numerical features as input, namely data['Fare'], data['Pclass'] and data['Age'], as shown below, 

.. code-block:: python

    >>> numerical_features = data[['Fare', 'Pclass', 'Age']]
    >>> numerical_features.head(5)
          Fare  Pclass   Age
    0   7.2500       3  22.0
    1  71.2833       1  38.0
    2   7.9250       3  26.0
    3  53.1000       1  35.0
    4   8.0500       3  35.0
    >>> 

* Note that, all the age information is not provided by all the passenger as shown below, 
  
 .. code-block:: python

     >>> nullAge = numerical_features[numerical_features['Age'].isnull()]
     >>> len(nullAge)
     177
     >>> nullAge.head(3)
            Fare  Pclass  Age
     5    8.4583       3  NaN
     17  13.0000       2  NaN
     19   7.2250       3  NaN
     >>>  

* Next, we will fill the 'NaN' values of 'Age', 'Fare' and 'PClass' with corresponding median values as shown below,  

.. code-block:: python
    
    >>> # remove NaN and calculate median
    >>> median_features = numerical_features.dropna().median()
    >>> median_features
    Fare      15.7417
    Pclass     2.0000
    Age       28.0000
    dtype: float64

    >>> # fill the NaN with median_features
    imputed_features = numerical_features.fillna(median_features)

* Now that the pandas DataFrame is clean, we can convert it into an homogeneous numpy array of floating point values, 

.. code-block:: python
    
    >>> features_array = imputed_features.values
    >>> features_array
    array([[  7.25  ,   3.    ,  22.    ],
           [ 71.2833,   1.    ,  38.    ],
           [  7.925 ,   3.    ,  26.    ],
           ..., 
           [ 23.45  ,   3.    ,  28.    ],
           [ 30.    ,   1.    ,  26.    ],
           [  7.75  ,   3.    ,  32.    ]])o

* In this way, we can convert any data into the format which can be used by scikit-learn library.

Training and testing data
-------------------------

Next, we will use 80% of the data as training data and remaining 20% of the data will be used to check the accuracy of the model. In the other words, we will make a models for 'survival after crash' using 80% data of the 'titanic_train.csv' file; and then the predictive behaviour will be checked for remaining 20% data i.e. we will check whether the model gives the correct information about the survival based on input provided or not. 

* 'train_test_split' can be used to split the data as training and testing set as shown below, 

.. code-block:: python

    >>> from sklearn.cross_validation import train_test_split

    >>> features_train, features_test, target_train, target_test = train_test_split(
    >>> ...     features_array, target, test_size=0.20, random_state=0)

    
    >>> features_train.shape
    (712, 3)
    >>> features_test.shape
    (179, 3)
    >>> target_train.shape
    (712,)
    >>> target_test.shape
    (179,)
   
* Now, we can choose any model from the scikit-learn library. In the following code, LogisticRegression is used to model the data, 

.. code-block:: python
    
    >>> from sklearn.linear_model import LogisticRegression
    >>> logreg = LogisticRegression(C=1)          # create a new model
    >>> logreg.fit(features_train, target_train)  # fit the data to model
    LogisticRegression(C=1, class_weight=None, dual=False, fit_intercept=True,
              intercept_scaling=1, max_iter=100, multi_class='ovr', n_jobs=1,
              penalty='l2', random_state=None, solver='liblinear', tol=0.0001,
              verbose=0, warm_start=False)


    # predict the result for test datat
    >>> target_predicted = logreg.predict(features_test)

    # predict the accuracy of the model
    >>> from sklearn.metrics import accuracy_score
    >>> accuracy_score(target_test, target_predicted)
    0.73184357541899436
    >>> logreg.score(features_test, target_test)
    0.73184357541899436

* The above model is 73% accurate, which is very good at all. First reason for less accuracy is the linear model. Second reason is that, we are using only 3 fields from the dataset to predict the data. 

Deducing information from model
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The 'coef\_' attribute of a fitted linear model such as 'LogisticRegression' holds the weights of each features. With the help of these weights we can deduce some useful information as shown below, 

.. code-block:: python

    >>> feature_names
    Index(['Fare', 'Pclass', 'Age'], dtype='object')
    >>> logreg.coef_
    array([[ 0.0043996 , -0.80916725, -0.03348064]])

.. note:: survival =  Fare*0043996 + Pclass*(-0.80916725) + Age*(-0.03348064).   If survival is positive, the person is considered to bs survived. 

* First element in logreg.coef\_ is positive value, which indicates that if passenger pay more then probability of his survival is more than others. Next, element indicates that higher the class, lower the probability of survival i.e. passenger in 3rd class have less chances of survival. Last column indicates that higher the age, lower the chance of survival. 

 This can be justified by this fact, "In this case, survival is slightly positively linked with Fare (the higher the fare, the higher the likelihood the model will predict survival) while passenger from first class and lower ages are predicted to survive more often than older people from the 3rd class."



 First-class cabins were closer to the lifeboats and children and women reportedly had the priority. Our model seems to capture that historical data. We will see later if the sex of the passenger can be used as an informative predictor to increase the predictive accuracy of the model. 
